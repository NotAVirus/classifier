import librosa, librosa.display

import os
import json
import math
import config


expected_num_mfcc_vectors = math.ceil(config.SAMPLES_PER_TRACK / config.HOP_LENGTH)

def save_mfcc(dataset_path, json_path):
    # data dictionary
    data = {}
    
    for i, (dirpath, dirnames, filenames) in enumerate(os.walk(dataset_path)):
        if(dirpath is not dataset_path):
            genre = dirpath.split("\\")[-1]
            print("processing",genre)
            data[genre] = {"mfcc": []};
            
            for file in filenames:
                file_path = os.path.join(dirpath, file) 
                signal, sr = librosa.load(file_path, config.SAMPLE_RATE, duration = config.DURATION)
                mfcc = librosa.feature.mfcc(signal, sr, n_mfcc=config.N_MFCC, n_fft=config.N_FFT, hop_length=config.HOP_LENGTH)
                mfcc = mfcc.T
                if(len(mfcc) == expected_num_mfcc_vectors):
                    data[genre]["mfcc"].append(mfcc.tolist())
                    
    with open(json_path, "w") as fp:
        json.dump(data, fp, indent=4)
    
    
save_mfcc(config.TRAINING_DATA_PATH, config.DATA_PATH)
print("finished classifying")