# Kurzbeschreibung
Ein Soundfile Klassifizierer mit 2 unterschiedlichen Methoden:
- classifier.py klassifiziert mithilfe von GMM
- classifier_tensorflow klassifiziert mithilfe eines Neuronalem Netzwerk

Es besteht hier bereits ein Training und Test-Sets von Tieren. im data.json finden sich auch bereits die extrahierten MFCCs.

# Weitere Features
Zusätzlich wurden einige nützliche Features programmiert:
- config.py beinhaltet die einzelnen Einstellungen und Pfade
- preprocessing.py Extrahiert MFCC Daten aus den Soundfiles und speichert diese für die Klassifizierer ab.
- sound_recorder.py Um eigene Sounds aufzunehmen
- live_sound.py um Live Audio Aufnahmen zu klassifizieren

