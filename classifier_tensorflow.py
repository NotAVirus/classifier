import numpy as np
import tensorflow.keras as keras
import classifier;
from sklearn.model_selection import train_test_split
import os
import matplotlib.pyplot as plt
import config


def get_labels(training_data_path):
    labels = []
    for i, (dirpath, dirnames, filenames) in enumerate(os.walk(training_data_path)):
        if(dirpath is not training_data_path):
            label = dirpath.split("\\")[-1]
            labels.append(label)
    return np.sort(labels)

def plot_history(history):
    fig, axs = plt.subplots(2)
    axs[0].plot(history.history["accuracy"], label="train accuracy")
    axs[0].plot(history.history["val_accuracy"], label="test accuracy")
    axs[0].set_ylabel("Accuracy")
    axs[0].legend()
    
    axs[1].plot(history.history["loss"], label="train loss")
    axs[1].plot(history.history["val_loss"], label="test loss")
    axs[1].set_ylabel("Error")
    axs[1].legend()
    
    plt.show()
    
def show_statistic(model, inputs, correct_labels):
    labels = get_labels(config.TRAINING_DATA_PATH)
    predicted_labels = model.predict_classes(inputs)
    for i in range(len(inputs)):
        predicted_label = labels[predicted_labels[i]]
        real_label = labels[correct_labels[i]]
        if(predicted_label == real_label):
            print(predicted_label,"correct")
        else:
            print("%s wrong, should be %s" % (predicted_label, real_label))


def create_model():
    data = classifier.load_data(config.DATA_PATH)
    labels = get_labels(config.TRAINING_DATA_PATH)
    
    training_mfccs = []
    training_labels = []
    i = 0;
    for genre in data.keys():
        for val in np.array(data[genre]["mfcc"]):
            training_mfccs.append(val)
            training_labels.append(i)
        i += 1
    
    training_mfccs = np.array(training_mfccs)
    training_labels = np.array(training_labels)
    inputs_train, inputs_test, labels_train, labels_test = train_test_split(training_mfccs, training_labels, test_size=0.1)
    
    model = keras.Sequential([
        keras.layers.Flatten(input_shape=(training_mfccs.shape[1], training_mfccs.shape[2])),
        keras.layers.Dense(config.HOP_LENGTH, activation="relu"), # rectified linear unit
        keras.layers.Dropout(0.3),
        
        keras.layers.Dense(config.HOP_LENGTH / 2, activation="relu"),
        keras.layers.Dropout(0.2),
    
        keras.layers.Dense(config.HOP_LENGTH / 4, activation="relu"),
    
        keras.layers.Dense(len(labels), activation="softmax")
    ])
    
    
    # adam is like gradient decent optimizer
    optimizer = keras.optimizers.Adam(learning_rate=0.0001)
    model.compile(optimizer=optimizer, loss="sparse_categorical_crossentropy", metrics=["accuracy"])
    history = model.fit(inputs_train, labels_train, validation_data=(inputs_test, labels_test), epochs=300, batch_size=32)
    plot_history(history)
    return { 'model': model, 'labels': labels, 'inputs_train': inputs_train, 'labels_train': labels_train, 'inputs_test': inputs_test, 'labels_test': labels_test }


def get_label_from_mfcc(model, mfcc):
    labels = get_labels(config.TRAINING_DATA_PATH)
    label = model.predict_classes([mfcc.tolist()])
    return labels[label[0]]

created_model = create_model()
show_statistic(created_model["model"], created_model["inputs_test"], created_model["labels_test"])
