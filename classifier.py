import json
import numpy as np
from sklearn import mixture
import sys
import os
import config

import librosa, librosa.display

SAMPLE_RATE = config.SAMPLE_RATE

DURATION = config.DURATION
SAMPLES_PER_TRACK = SAMPLE_RATE * DURATION

N_MFCC = config.N_MFCC
N_FFT = config.N_FFT
HOP_LENGTH = config.HOP_LENGTH

def build_models_from_data(data_path):
    print("loading data sets...")
    training_set = load_data("data.json")
    print("extracting gmm for data_sets")
    training_models = extract_models_per_genre(training_set)
    print("training models loaded...")
    return training_models

def load_data(data_path):
    with open(data_path, "r") as fp:
        data = json.load(fp)
    
    return data

def build_model(features):
    # mean_mfcc = features.mean(axis=0)
    vectors = []
    for feature in features:
        for row in feature:
            vectors.append(row)
            
    gmm = mixture.GaussianMixture(n_components=13, covariance_type='spherical', n_init=1)
    gmm.fit(vectors)
    return gmm

def extract_models_per_genre(training_set):
    genres_to_model = []
    for genre in training_set:
        print("build model for genre:", genre)
        
        gmm = build_model(np.array(training_set[genre]["mfcc"]))
        genres_to_model.append({'genre': genre, 'gmm': gmm})
    return genres_to_model

def get_mfccs_from_file(file):
    signal, sr = librosa.load(file, SAMPLE_RATE, duration=DURATION)
    mfcc = librosa.feature.mfcc(signal, sr, n_mfcc=N_MFCC, n_fft=N_FFT, hop_length=HOP_LENGTH)
    mfcc = mfcc.T
    return mfcc

def score_model(test_mfccs, training_models):
    best_score =  -sys.float_info.max
    for training_model in training_models:
        score_per_featurevector = training_model['gmm'].score(test_mfccs)
        score = np.sum(score_per_featurevector)
        if score > best_score:
            best_score = score
            best_match = training_model

    return best_match["genre"]

def test_set(dataset_path):
    training_models = build_models_from_data("data.json")

    '''
    file = input("Filename: ")
    while(file != "\n"):
        print("Load and extract file to test")
        
        test_mfccs = get_mfcc_from_file(file)
    
        print("score test file...")
        guessed_genre = score_model(test_mfccs, training_models)
        print(guessed_genre)
        file = input("Filename: ")
        
    '''
    correct = 0
    wrong = 0
    count = 0
    for i, (dirpath, dirnames, filenames) in enumerate(os.walk(dataset_path)):
        if(dirpath is not dataset_path):
            genre = dirpath.split("\\")[-1]
            print("processing",genre)
     
            for file in filenames:
                file_path = os.path.join(dirpath, file)
                test_mfccs = get_mfccs_from_file(file_path)
                guessed_genre = score_model(test_mfccs, training_models)
                count += 1
                if(guessed_genre != genre):
                    print(guessed_genre," wrong. correct is: ", genre)
                    wrong += 1
                else:
                    print(genre,"correct")
                    correct += 1
    print("Korrekt:",correct)
    print("Falsch:",wrong)
    print("Prozent richtig: ",(correct / count))

#test_set("digit_training_data")