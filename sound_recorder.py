import sounddevice as sd
from scipy.io.wavfile import write
import config

fs = config.SAMPLE_RATE
seconds = config.DURATION
folder = "kevin_test"

# sentences: https://en.wikipedia.org/wiki/Harvard_sentences
input("press Enter to start recording")
for i in range(1,50):
    filename = f'{folder}/test2{i}.wav'
    recording = sd.rec(int(seconds * fs), samplerate=fs, channels=1)
    sd.wait()
    f = open(filename, "a")
    f.close()
    write(filename, fs, recording)  # Save as WAV file 
    print(i)
print("finished")