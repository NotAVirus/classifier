# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 10:08:15 2020

@author: Kevin
"""
import sounddevice as sd
import numpy as np
import classifier
import librosa
import matplotlib.pyplot as plt
import config

import classifier_tensorflow

training_models = classifier.build_models_from_data("data.json")
keras_training = classifier_tensorflow.create_model()

while(True):
    recording = sd.rec(int(config.DURATION * config.SAMPLE_RATE), samplerate=config.SAMPLE_RATE, channels=1, device=1)
    sd.wait()
    recording = np.squeeze(recording)
    
    if(max(recording) > 0.08):
        test_mfcc = np.array(librosa.feature.mfcc(y=recording, sr=config.SAMPLE_RATE, n_mfcc=13).T)
        print(classifier.score_model(test_mfcc, training_models))
        print(classifier_tensorflow.get_label_from_mfcc(keras_training["model"], test_mfcc))
